<?php

namespace Drupal\ai_translator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\locale\StringDatabaseStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an Ai translator form.
 *
 * @see https://www.codimth.com/blog/web/drupal/how-use-batch-api-drupal-8-9
 */
class TranslatorForm extends FormBase {

  protected StringDatabaseStorage $localeStorage;
  protected LanguageManagerInterface $languageManager;

  public function __construct(
    StringDatabaseStorage $localeStorage,
    LanguageManagerInterface $languageManager
  ) {
    $this->localeStorage = $localeStorage;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('locale.storage'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_translator_translator';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $langCodes = $this->languageManager->getLanguages();

    $options = [];
    foreach ($langCodes as $langIso => $langCode) {
      $options[$langIso] = $langCode->getName();
    }

    $form['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Select destination language'),
      '#required' => TRUE,
      '#options' => $options,
    ];

    $form['not_translated_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Untranslated literals only?'),
      '#default_value' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->localeStorage;
    $notTranslatedOnly = $form_state->getValue('not_translated_only');
    $conditions = ['language' => $form_state->getValue('language')];
    if ($notTranslatedOnly === 1) {
      $conditions['translated'] = FALSE;
    }
    else {
      $conditions['translated'] = TRUE;
    }
    $literals = $storage->getStrings($conditions);
    $operations = [];
    /** @var \Drupal\locale\SourceString $literal */
    foreach ($literals as $literal) {
      $operations[] = ['ai_translator_translate_literal', [$form_state->getValue('language'), $literal->source]];
    }
    $batch = [
      'title' => $this->t('Translating ...'),
      'operations' => $operations,
      'finished' => 'ai_translator_translate_literal_finished',
    ];
    batch_set($batch);
  }

}
